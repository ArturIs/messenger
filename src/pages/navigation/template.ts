export default `
  <nav class="navigation__panel">
    {{{ links }}}
  </nav>
`;
